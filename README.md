# Stream manipulator auto exec
This dead simple header only library allows ```ostream``` objects to
automatically execute custom stream manipulators.

## Legal
This library is free software. It comes without any warranty, to the extent
permitted by applicable law. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2, included
in the LICENSE.md file.

## Future
I consider this project to be complete.
