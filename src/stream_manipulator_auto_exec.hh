#ifndef STREAM_MANIPULATOR_AUTO_EXEC_STREAM_MANIPULATOR_AUTO_EXEC_HH
#define STREAM_MANIPULATOR_AUTO_EXEC_STREAM_MANIPULATOR_AUTO_EXEC_HH

#include <functional>
#include <ostream>

inline std::ostream &operator<<(
  std::ostream &os, std::function<std::ostream&(std::ostream&)> &f
) {
  return f(os);
}

inline std::ostream &operator<<(
  std::ostream &os, std::function<std::ostream&(std::ostream&)> &&f
) {
  return f(os);
}

#endif
